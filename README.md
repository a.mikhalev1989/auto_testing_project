

# Acceptance test project

 Test project for acceptance testing

# Install

## Git clone

### HTTPS

``$ git clone https://gitlab.com/a.mikhalev1989/auto_testing_project.git ``

### SSH

``$ git clone git@gitlab.com:a.mikhalev1989/auto_testing_project.git``

## Installation of dependencies

`$ bundle install`

## Running tests

``$ rake all_tests``
